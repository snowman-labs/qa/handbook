---
title: Handbook
date: 2018-05-08 00:00:00
catalog: false
layout: home
---

# Descrição

Esta é uma página central de como as coisas funcionam na Snowman Labs. Como parte de nossos valores é ser transparente, esse Handbook é aberto para o mundo e convidamos a dar seu feedback sobre. Por favor, faça um merger request para sugerir melhorias e correções. Para dúvidas e problemas, por favor, abra uma issue.

* Geral
    * Valores
* Pessoas
    * Comunicação
    * Código de Conduta
    * Liderança
    * Contratação
    * Onboarding
* [Desenvolvimento](desenvolvimento)
    * [Papéis e Responsabilidades](desenvolvimento/papeis-e-responsabilidades.html)
    * Infraestrutura
    * [Garantia de Qualidade](desenvolvimento/qa)
        * [Garantindo a Qualidade do seu Projeto](desenvolvimento/qa/garantindo-a-qualidade-do-seu-projeto.html)
        * [Mensagem de Commit](desenvolvimento/qa/mensagem-de-commit.html)
        * [Controle de Versão](desenvolvimento/qa/controle-de-versao.html)
        * [Code Review](desenvolvimento/qa/code-review.html)
        * [Trabalhando com Issues](desenvolvimento/qa/trabalhando-com-issues.html)
        * [Fluxo de Branch e Releases](desenvolvimento/qa/fluxo-de-branchs-e-releases.html)
        * [Execução de Testes](desenvolvimento/qa/execucao-de-testes.html)
        * [Comunicação](desenvolvimento/qa/comunicacao.html)
* Marketing
    * Design
        * UX Design
        * UI Design
* Vendas
* Financeiro
* Juridico
* Outros
