# Code Review

O code review (revisão de código) é uma atividade de garantia de qualidade de software em que uma ou mais pessoas verificam o código fonte de uma aplicação, que ajuda a encontrar melhorias, bugs e oferecer uma melhor qualidade de software como um todo. Além disso, o Code Review é

> um processo colaborativo e é imprescindível entender que críticas são necessárias, mas devem ser feitas com respeito e visando sempre aprimorar a solução apresentada.

> O code review não é apenas uma tarefa a ser executada, ele é parte da cultura da empresa, pois envolve pessoas. Antes de saber como fazer code review é importante entender porquê fazê-lo.

## Beneficios

Dentro de uma equipe trabalhando com code review, podemos ter os seguintes benefícios:

- Criação de soluções alternativas para os problemas;
- Compartilhamento de conhecimento;
- Aumento do senso de equipe;
- Melhorar a qualidade do software como um todo;

Quando trabalhamos em equipe e outra pessoa revisa nosso código, encontramos soluções alternativas pra um problema que pode nos ajudar e ver o código por outro ponto de vista e compreender melhor o uso da linguagem, ferramenta ou técnica. Isso sem falar que a equipe acaba ficando mais unida com o tempo, por conhecerem melhor como o companheiro trabalha.

## Responsabilidades

O **Reviewer** deve garantir que a solução apresentada atende aos requisitos funcionais e técnicos. Apresentando outra perspectiva sobre o problema e a solução quando necessários.

O **Developer** deve estar aberto as críticas apresentadas à sua solução, além disso, estar preparado para agir sanando as dúvidas e aplicando alterações necessárias identificadas pelos reviewers.

## Ciclo de Code Review

- Desenvolvedor realiza as mudanças e melhorias necessárias dentro de uma nova branch baseada na issue
- Quando terminar e subir as mudanças para o servidor de git, o desenvolvedor mesmo solicita um MR (Merge Request)
- O reviewer que foi marcado faz a revisão das mudanças realizadas em cima do que foi pedido na issue
- Além de analisar o que foi pedido na issue faz analise do código e afins. Veja [O que revisar/analisar?](#O-que-analisar-durante-o-Code-Review)
- Após o developer subir todas as mudanças e o reviewer não ter mais nada a reportar a revisão está concluída

## Preparando seu MR para Code Review

- Buscar outras issues que seu MR (Merge Request) pode estar relacionado, marcando-as para serem fechadas, ou apenas citando-as se não for o caso de fechar.
- Adicionar toda informação necessária para explicar as coisas que esse MR altera.
- Se necessário, descrever como executar novos procedimentos (ex.: deploy, test, migrations, etc.)
- Se necessário, explicar os decisões tomadas e motivos.
- Marcar o(s) grupo(s) de reviewer(s) adequados (ex.: `@snowman-labs/be-devs`)
- Marcar como assignee o maintainer do projeto (ex.: o tech lead, ou outro dev do projeto com esse papel)
- Remover WIP (Work In Progress)
- Quando estiver realizando as mudanças solicitadas pelo Reviewer add o WIP (Work in Progress) novamente e remover quando terminar.

## Fazendo Code Review de um MR

- Inicie adicionando um comentário de que já está fazendo o Code Review do MR
- Buscar se já não tem outra issue e/ou MR relacionado, marcando-as para serem fechadas, ou apenas citando-as se não for o caso de fechar
- Caso não tenha passado nos requisitos do CI ou ainda esteja com WIP não analisar e informar o desenvolvedor sobre
- Analisar:
  - Adequação quanto as regras da Stack (code style, arquitetura, dependências, etc.)
  - Cumprimento do objetivo
  - Clareza da solução
  - Cobertura de testes
  - Pontos de melhoria
  - Possíveis problemas não previstos pela proposta
- Quando terminar de revisar, faça um comentário marcando o desenvolvedor para que ele possa aplicar as mudanças
- Ao terminar o Code Review e tudo estando OK adicione um 👍 para indicar que aprovou tudo

## Boas Práticas

- Automatize parte do processo de review, execute testes unitários, coverage, dependências, e outros
- Se não passou pela automação não tem o que ser revisado
- Ter um reviewer que não seja o autor do código
- Não faça comentários pessoais na review, seja técnico
- Não tome como pessoal comentários feitos ao seu código ou solução
- Seguir aos padrões de código do projeto, se não tem, precisa ter
- Pair programming não é code review
- Como developer, entenda o que precisa ser feito para apresentar boas soluções
- Como reviewer, questione mais sobre as mudanças ao invés de só ficar pedindo alterações e correções
- Insista na qualidade das revisões, mas concorde em discordar, ou seja, discuta sempre que possível

## Entendendo alguns pontos

Faça críticas construtivas, evite criticas depreciativas que não ajudarão em nada. Para ajudar no processo de comunicação, finalize comentários com "*por favor*", quando parecer uma ordem ou pergunte coisas como "*Já considerou fazer X de um jeito Y?*", "*Será que isso funciona se fizer X?*". Se possível questione, seja educado e encontre formas alternativas de explicar seu ponto.

Não implemente a cultura do medo aos reviewers, pois senão ninguém fará nada e ficarão desmotivados, ao invés disso, motive a discussão. Assim o developer vai se perguntar coisas como: "*O que é melhor ...?*", "*O que o reviewer vai achar se ...?*" que são perguntas que ajudará a tentar pensar na solução de uma forma diferente e por consequência até melhor o processo de review.

Um code review não é pra monitorar trabalho de ninguém, mas sim melhorar o trabalho de todos como uma equipe. Quando compreendemos o trabalho de um companheiro, todos saem ganhando, pois todos aprendem e evoluem juntos.

Tire um tempo para ensinar, não somente dizer o que fazer. Esse simples pensamento pode ajudar a motivar uma melhor cultura de code review.

Tente mostrar coisas positivas, além de apontar melhorias, aponte os sucessos do código ou da lógica usada. Por pior que o código esteja sempre tem algo positivo pra ressaltar.

## Links e Referencias

[Code Review - Wikipedia](https://en.wikipedia.org/wiki/Code_review)

[A importância do Code Review para a equipe de desenvolvimento de software](https://medium.com/equals-lab/a-importancia-do-code-review-para-a-equipe-de-desenvolvimento-de-software-a47b70cb1560)

[Ciclo de Code Review](https://mtlynch.io/images/human-code-reviews-1/flowchart.png)

[Cultura do Medo de Code Review](https://cdn-images-1.medium.com/max/1600/1*97h2kI0TQG6P0AO2IVvOBw.png)
