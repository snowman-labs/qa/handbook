---
title: Mensagem de Commit
p: desenvolvimento/mensagem-de-commit.md
date: 2019-05-128 09:50:05
---

# Mensagem de Commit

Esse guia irá ensinar a escrever uma mensagem de commit e a importância dela. Além disso, você vai entender porque é importante escrever uma boa mensagem usando boas práticas e conhecer dicas para planejar e (re)escrever o seu histórico de commits.

Todas as mensagens de commit devem ser escritas em português do brasil (pt-BR).

## O que é um "commit"?

Em termos simples, o commit é uma espécie de snapshot dos seus arquivos locais gravado localmente no seu repositório. Ao contrário do que se pensa, o git não armazena apenas as diferenças e sim a cópia completa dos arquivos. No caso de arquivos que não mudaram de um commit para o outro, é gravada uma referência ao arquivo gerado no último snapshot.

## Por que as mensagens são importantes?

- Facilitam e agilizam o code review
- Ajudam no entendimento do que está acontecendo
- Explicam os porquês ocultos que não podem ser explicados somente em código
- Facilitam a solução de problemas e a depuração garantindo que futuros codificadores entendam **por que** e **como** as mudanças foram feitas

## Boas práticas

### Use o Imperativo

```
# Bom
Usa InventarioBackendPool para listar inventários do Backend
```

```
# Ruim
Usado InventarioBackendPool para listar inventários do Backend
```

**Por que!?**

A mensagem de commit diz o que ele **faz**, não o que foi feito.

```
Se aplicado, este commit vai: <mensagem de commit>
```

**Regrinha pra ajudar a visualizar melhor a mensagem**

```
# Bom
Se aplicado, este commit vai: Usar InventarioBackendPool para listar inventários do Backend
```

```
# Ruim
Se aplicado, este commit vai: Usado InventarioBackendPool para listar inventários do Backend
```

## O que o commit faz?

Tente comunicar o que o *commit* faz sem que seja necessário olhar o conteúdo dele

```
# Bom
Adiciona metodo `use` ao Model de Credito
```

```
# Ruim
Adiciona metodo `use`
```

```
# Bom
Incrementa o preenchimento esquerdo entre a caixa de texto e o frame de layout
```

```
# Ruim
Ajuste de CSS
```

## Use o corpo da mensagem para explicar *"porquê"*, *"para quê"*, *"como"* e detalhes adicionais

Modelo que pode te ajudar a entender melhor como fazer:

- Título
- Problema
- Solução
- Referências/Issue Tracker

Sendo o titulo o assunto do commit e os demais o corpo da mensagem de commit. Lembre-se sempre de separar os parágrafos com uma linha em branco.

**Tente manter o tamanho das colunas em 72 caracteres, pois fica mais fácil de ler em qualquer tela e ambiente**

Exemplo:

```
# Bom
Usa InventarioBackendPool para listar inventarios do Backend

Descreva aqui o problema que esse commit resolve. Não descreva a
solução, somente o problema nesse paragrafo. Se necessário pode usar
mais de uma paragrafo.

Descreva a seguir a solução adotada e qual o motivo de usar a mesma.
Tente não descrever tanto o código, mas sim o motivo e como resolveu.

Abaixo informe a Issue e outras referências, como abaixo:

Resolve: #1
Veja tambem: #123, #321
referências: http://url-para-algo-que-ajude (se necessário)
```

**Emojis são permitidos desde que sejam usados só para complementar as mensagens, nunca substituir.**

## Comandos Git

A seguir alguns comandos em git que podem ajudar a visualizar melhor o processo e conseguir seguir esse padrão de mensagens de commit:

```bash
# exibe um mapeamento de histórico de commits contendo commit_id e o sumario/assunto do commit
git log --pretty=oneline
```

```bash
# oferece um sumario/assunto de cada commit e abre no editor
# se a opção de `merge.summary` estiver configurada no git,
#   o sumario para todos os commits mergeados vão aparecer dentro da mensagem de commit do merge
git rebase --interactive
```

```bash
# produz uma saída de logs sumarizada em linhas no estilo changelog
git shortlog
```

```bash
# ferramenta gráfica e simples do git que tem um coluna somente para sumários/assuntos dos commits
gitk
```

## Conclusão

Em resumo podemos apontar os itens abaixo, que descrevem uma boa mensagem de commit dentro de todo o processo.

- Todas as mensagens de commit devem ser em português do Brasil
- Limite a linha do assunto em até 72 caracteres
- Use descrições mais imperativas na linha do assunto
- Não precisa terminar a linha do assunto com ponto final (.)
- Separe o assunto do corpo com uma linha em branco
- Quebre as linhas do corpo da mensagem com até 72 caracteres
- Use o corpo da mensagem para explicar *"porquê"*, *"para quê"*, *"como"* e detalhes adicionais
- Mensagem de commit deve ter: *Título; Problema; Solução; Referências/Issue Tracker*
- Emojis são permitidos, mas não substitui a mensagem