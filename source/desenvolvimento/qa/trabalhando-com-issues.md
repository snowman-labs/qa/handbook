# Trabalhando com Issues

Aqui entra nosso processo de uso das Issues dentro do Gitlab na Snowman Labs. Entenda melhor o fluxo para fazer melhor uso das ferramentas que ajudam a ganhar tempo.

# Issues

Por meio das issues podemos abrir tarefas, propostas de melhorias num código, infraestrutura ou ferramenta, sugerir documentações, tirar dúvidas mais avançadas que ajudem a todos e muitas outras coisas. Esse processo de trabalho com a issue nos ajuda a ganhar tempo para entender melhor do que se trata o problema e trabalhar de forma mais assertiva em oferecer soluções.

**Para abri uma issue você precisa informar:**

- Titulo
- Descrição (use os templates para ganhar tempo)
- Atribuir quem precisa trabalhar ou saber sobre
- Atribuir as Labels de acordo com as regras do projeto

**Sempre que possível tente:**

- Anexar arquivos e imagens que podem ajudar a compreender o escopo
- Informar tempo e prazo baseado no processo de Scrum feito previamente
- Linkar para outras issues e MR (caso seja preciso, para manter as referencias)

Caso seja preciso fragmentar tarefas dentro do projeto, crie novas issues lincando a original. Como sempre, verifique as regras e necessidades do projeto antes de fragmentar em várias issues, pois o uso de ToDo dentro da issue pode ser uma forma simples de resolver.

Por meio de uma issue você pode **Criar Merge Requests e Branchs** facilmente. Porém, fique atento ao fazer isso para não esquecer de usar o template de **Code Review** no MR, pois ele ajuda muito nessa prática.

Exemplo de uma issue aberta:

![Tela Principal de uma Issue](https://docs.gitlab.com/ee/user/project/issues/img/issues_main_view.png)

## Issue Boards

Issue Boards são uma forma de visualizar as issues do projeto em forma de Kanban, usando as tags de cada issue como forma de agrupá-las, facilitando assim o processo de desenvolvimento e tornando-o mais visual.

Abaixo um exemplo de Issue Boards em forma de kankan:

![Tela de uma Issue Board](https://docs.gitlab.com/ee/user/project/img/issue_boards_core.png)

Saiba mais sobre a funcionalidade de Issue Boards em: [Issue Boards - Gitlab](https://docs.gitlab.com/ee/user/project/issue_board.html)

## Principais Dúvidas

### Ao abrir nova issue preciso usar o template?

Sempre que possível sim, porém caso o conteúdo não se encaixe em nenhum dos templates, [proponha um template](http://gitlab.com/snowman-labs/templates/template-base). Mas até que o template correto não exista, prossiga com a abertura da issue passando o máximo de informações possíveis, ajudando quem irá trabalhar nela, ou revisá-la, a entender melhor do que se trata.

### Posso abrir issue para tirar dúvidas?

Sempre que possível tente sanar suas dúvidas lendo a documentação do projeto, seja na [Wiki](https://docs.gitlab.com/ee/user/project/wiki/), no [Postman](https://snowmanlabs.postman.co), no [Handbook da Snowmanlabs](https://handbook.snowmanlabs.com/) ou documentação fornecida pelo cliente. Se você não encontrar a resposta, utilize o canal do **Slack** do projeto para assuntos simples ou de pouco impacto nas decisões do projeto. Caso seja uma informação bastante importante para outros membros do grupo ou seja uma decisão a ser tomada, abra uma issue para discutir e sanar suas dúvidas e aproveite para documentar o resultado.

**Sempre documente informações e decisões importantes e os motivos.**

### Quais melhores situações para usar as Issues?

- Discutir a implementação de uma idéia
- Gerenciar tarefas ou status do trabalho
- Aceitar propostas de melhorias, questões, solicitações de suporte, e relatar erros (bugs)
- Elaborar a implementação de novos códigos

### Tem como gerenciar micro tarefas (ToDo) dentro da Issue?

Sim. É bem simples de usar como em outros gerenciadores de repositórios, basta usar o código abaixo:

```markdown
- [ ] Item um que precisa ser feito
- [ ] Item dois, já resolvendo
```

### Posso alterar uma issue ou move-la?

Sim, você pode. Porém, veja antes se tem alguma restrição no projeto para isso. Pois é possível mover, editar, deletar e outras funcionalidades numa issue.

### Posso fazer uso de emojis?

Use de forma consciente. Lembre-se, emojis e ícones não podem substituir uma mensagem, somente complementar o que está sendo dito. Além disso, veja as regras do projeto.

## Referencias

[Issue - Gitlab](https://docs.gitlab.com/ee/user/project/issues/)
[Issue Boards - Gitlab](https://docs.gitlab.com/ee/user/project/issue_board.html)