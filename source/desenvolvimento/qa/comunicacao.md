# Comunicação

Como a própria definição diz, comunicação é um processo que envolve a troca de informações entre dois ou mais interlocutores, mas que também requer o entendimento de todas as partes. Então iremos ver as principais formas de comunicação usadas na Snowman Labs e como podem ser feitas.

## Canais de Comunicação

Os canais a seguir são os utilizados na Snowman Labs em comunicação interna ou com os clientes. A forma de comunicação síncrona é a mais importante no retorno ou na forma de comunicar-se na Snowman Labs.

**Slack**\
`comunicação síncrona`

Ferramentas para centralizar conversas e retornos de outras ferramentas externas. Além de ser a forma de se comunicar internamente na Snowman Labs e uma das formas de comunicação com o cliente.
Geralmente o Slack terá uma conversa mais informal, e sendo assim, é seguir algumas diretivas pra ajudar a todos: não use como forma de documentação; documente no Wiki ou README as decisões apresentadas no Slack do projeto no GitLab; não compartilhe informações pessoais ou sensitivas da empresa; não tome decisões de forma privada, compartilhe com os outros membros da equipe;

**WhatsApp**\
`comunicação assíncrona`

O grupo da Snowman Labs é utilizado para assuntos gerais da empresa e informações que ajudam na convivência da equipe.
Quando for grupo de projetos de clientes, vai ser definido de acordo com a equipe junto ao cliente dentro da necessidade desse grupo.

**E-Mail**\
`comunicação assíncrona`

Este meio de comunicação é utilizado na Snowman Labs para transmitir informações com conteúdos mais formais, sendo bastante utilizado pelos setores da Diretoria, RH, financeiro e para resolver dúvidas administrativas. Algumas ferramentas utilizadas na Snowman Labs, também usam o e-mail para notificar os colaboradores de inclusão, alteração e resolução de atividades e afins.

Tente sempre enviar um email por assunto, pois pode causar mais demora na resposta por ter que tentar responder tudo.

**Service Desk**\
`comunicação assíncrona`

Será utilizado apenas para SLA, onde pode se tornar uma issue posteriormente.


**Issue e Merge Requests**\
`comunicação assíncrona`

O uso de Issues e Merge Request também são uma forma de comunicação onde por meio de comentários/notas pode-se tirar dúvidas referente a Issue e/ou Merge Request aberta, assim como fazer uma sugestão dentro do contexto da issue ou MR. No entanto, dúvidas gerais que podem ser comuns a todos devem está documentadas na Wiki ou README do projeto dentro do GitLab.

Antes de abrir uma nova issue ou MR verifique se já não existe algo similar ao que está querendo, pois já pode ter sido questionado ou solicitado.

**Commit Messages**\
`comunicação assíncrona`

Devem ser utilizadas para comunicar contexto junto ao código que é inserido no projeto, tornando o entendimento muito mais fácil.

Para saber mais sobre as mensagens de commit acesso o [Handbook - Mensagem de Commit](https://handbook.snowmanlabs.com/desenvolvimento/qa/mensagem-de-commit.html)

**Audio e Video Conferencia**\
`comunicação síncrona`

Por ventura, a equipe pode adotar o uso de ferramentas de conferencias de vídeo e áudio como [Zoom](https://www.zoom.us), [Talky](https://talky.io/), Google Hangout, Microsoft Skype, e outros, sendo assim, fique atento em ter um ambiente para que consiga se comunicar, onde você entende o que é dito e é entendido, para evitar problemas de comunicação e de mal entendido.

Quando tiver problemas no seu ambiente de comunicação, tente procurar meios alternativos para facilitar esse processo. Ou seja, quando não puder falar via vídeo chat ou áudio chat, tente ir pra outro lugar e falar via chamada de telefone por exemplo.

Então seja consciente e busque essa compreensão para o cliente em áudio e vídeo conferencias.

Você pode ler o guia do Gitlab que pode servir como um guia para vídeo conferencias. [Gitlab Video Chats](https://about.gitlab.com/handbook/communication/#video-calls)

**Verbal - P2P - Ou presencial**\
`comunicação síncrona`

Comunicação **feita pessoalmente de pessoa para pessoa**. Nesse caso a pessoas estão próximas e podem ajudar a tirar a dúvida sem ferramentas, e mesmo assim as **decisões tomadas devem ser documentadas** e reportadas aos demais membros da equipe.

**Documentação**\
`comunicação assíncrona`

Melhor forma de comunicar uma decisão tomada no projeto e deixar registrados (no Wiki, README e outros) para todos os membros do projeto, ou seja, é uma forma muito efetiva quando utilizada para registrar decisões importantes e motivos para alterações.

## Código

O código também é uma forma de se comunicar com outros desenvolvedores e com a Stack, sendo assim, se preocupe em deixar o seu código o mais claro e lógico possível, para que ao dar suporte ao código criado ou mesmo fazer review, entenda a lógica do que foi ou está sendo feito.

Verifique qual será o Code Style do seu projeto e siga-o que irá lhe ajudar muito.

## Conversas Paralelas

Quando estiver em conversas que compreendam somente a 1 ou 2 indivíduos e não está tão ligado com o projeto, o ideal é que se comuniquem diretamente, por meio de qualquer um dos canais já citados. Podemos listar algumas conversas como:

- Conversas no estilo de cafézinho (onde se fala sobre qualquer coisa)
- Hobbies e interesses pessoais
- Esportes, Religião e outros
- Dentre outros itens não pertinentes aos projetos de forma direta

Lembrando que conversas paralelas são somente um problema quando discutida nos lugares incorretos, isso pode ser feito, mesmo num trabalho remoto, mas utilize os locais apropriados para isso. Veja mais em [Hora do Café](#Hora-do-Café).

## Hora do Café

Essa pode ser uma forma descontraída de dar um tempo para pensar e relaxar a cabeça. É praticamente o tempo que levamos pra pegar um café, paramos pra conversar com alguém e mudar o foco. A idéia é que consigamos fazer tanto pessoalmente quanto online, por meio do canal de comunicação que for melhor, para que ninguém fique excluído, seja no caso de trabalho remoto ou de pessoas com menos desenvoltura social.

Esta é uma ótima chance de conhecer com quem você trabalha, falar sobre coisas do dia a dia e compartilhar um café, chá ou bebida favorita. Isso não só irá ajudar você a fazer e construir amizades, como criar um ambiente mais confortável e amigável.

## Reuniões

### Daily

Quando estiver em uma Daily, comunique sua idéia de forma sucinta e clara. A idéia de uma daily é ser rápida e que todos fiquem cientes e compreendam o que está acontecendo no projeto.

### Inicio e Fim de Sprint ou Retrospectivas

Essas reuniões podem vir a demorar um pouco mais por ter que abordar mais temas e fazer uma analise melhor do que vai ser ou foi feito. Pode ter que responder perguntas como: 

- O que foi bem feito nesse período?
- O que não foi feito nesse período?
- O que podemos fazer melhor?

## Dicas ao se Comunicar

- Seja sucinto, evite enviar mensagens ou e-mails longos ou cheio de falácias
- Seja educado
- Releia antes de enviar sua mensagem
- Escute antes de falar/ler
- Pense antes de falar/ler
- Quando tiver que mandar textos longos, sempre que possível, enfatize os trechos do texto mais importantes
- Não envie mensagens fragmentadas ou demoradas
- Sempre que repetir uma mensagem, repita com outras palavras
- Antes de compartilhar um link, verifique se o mesmo está acessível e se o destinatário tem permissão de acesso
- Não envie informações para quem não deveria ter acesso, isso pode se tornar um problema de segurança da informação
- Mantenha a conversa positiva, amigável, real e produtiva enquanto você adiciona valor
- Agradeça quando trabalho bem feito, e não esqueça de marcar a pessoa
- Aprecie sugestões e feedbacks
- Não faça promessas que não pode cumprir ou manter

## Referencias

[Teoria da Comunicação](https://pt.wikipedia.org/wiki/Teoria_da_comunica%C3%A7%C3%A3o)

[Comunicação](https://pt.wikipedia.org/wiki/Comunica%C3%A7%C3%A3o)

[Gitlab - Communication (em inglês)](https://about.gitlab.com/handbook/communication/)

[Donut - Bring people together - (em inglês)](https://www.donut.com/pairing/)

[LinkedIn - 5 fun strategies companies are using to make remote workers feel included](https://business.linkedin.com/talent-solutions/blog/employee-engagement/2019/strategies-companies-use-to-keep-remote-workers-feeling-included)

[Tirinha XKCD (em inglês)](https://xkcd.com/1179/)

[Regras de Rapoport (em inglês)](https://rationalwiki.org/wiki/Rapoport%27s_Rules)
