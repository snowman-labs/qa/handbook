# Fluxo de Branchs e Releases

Durante o processo de desenvolvimento você pode precisar entender melhor o fluxo das branchs e releases da Snowman Labs. Essa página irá servir como guia, mas é sempre bom revisar com o Stack Lead e o Tech Lead o fluxo usado no projeto, pois cada projeto pode vir a ter suas particularidades.

Essa página não vai explicar o fluxo básico do Git caso ainda tenha dúvidas sobre isso veja os links nas referencias.

Abaixo segue uma imagem do padrão de fluxo de trabalho no git na Snowman Labs.

<figure style="text-align: center;">
    <a href="/images/snowman-labs-git-fluxo-de-trabalho-v1.jpg" title="Snowman Labs - Fluxo de Trabalho no Git (Clique para ver imagem completa)" target="_blank">
        <img src="/images/snowman-labs-git-fluxo-de-trabalho-v1.jpg" alt="Snowman Labs - Fluxo de Trabalho no Git" style="width:450px;">
    </a>
    <figcaption>
        Arte por 
        <a href="https://www.behance.net/gabrielstocco" title="Snowman Labs - Gabriel Stocco" target="_blank">
            Snowman Labs - Gabriel Stocco
        </a>
    </figcaption>
</figure>

### Branch Master

A branch master será a branch principal do projeto, onde tudo em desenvolvimento deverá ficar e será baseado, exceto quando forem releases ou hotfix. Essa branch fica como protected por padrão, aumentando a segurança de mudanças nessa branch.

### Branch Feature

A branch de features se refere as branchs criadas para serem trabalhadas nas issues criadas ou algo novo sendo desenvolvido. Ou seja, uma branch de feature geralmente irá seguir o padrão do Gitlab de `<numero_issue>-<titulo-da-issue>` o que irá ajudar a sempre correlacionar a branch com a Issue em que está sendo trabalhada mesmo antes de ter o MR (Merge Request) criado ou mesmo de ter sido feito algum commit.

### Branch Release

É uma branch fixa onde tudo que estiver pronto pra ser enviado pra produção irá ficar, porém atente-se que é preciso adicionar a Tag de versão ao commit dessa Release. Além disso, atente a [Branch de Hotfix](#Hotfix). Assim como a branch master, a branch de release pode ser definida como protected para aumentar a segurança do desenvolvimento.

### Branch Hotfix

Quando forem encontrados problemas após uma release ter sido criada e precisar entrar na Release, a branch de Hotfix precisa ser criada baseada na última versão da release que se deseja alterar, pois assim quando for criado o MR serão criados 2 MR onde uma será para branch `Master` e outra pra branch de `Release`. Quando novo MR for feito na branch de Release lembrar de criar nova tag de versão.

No caso de a hotfix não ser de uma release ou poder entrar na próxima release ela poderá seguir o fluxo de uma branch de Feature.

### Tag

A cada nova release criada, será preciso definir uma tag de versão para o commit na branch de Release que deve seguir o padrão de [Controle de Versão](/desenvolvimento/qa/controle-de-versao.html). Lembrando que outras tags podem ser usadas de acordo com as necessidades do projeto, mas sempre revise com o Teck Lead e o Stack Lead do projeto.

## Dicas importantes

- Por padrão a branch master é protected não permitindo qualquer um alterar a mesma, também pode ser aplicado a branch release ou outras;
- As mudanças realizadas devem sempre ficar no Gitlab, o fluxo de trabalho é sempre online;

## Outros Tópicos Importantes

- [Mensagem de Commit](desenvolvimento/qa/mensagem-de-commit.html)
- [Controle de Versão](desenvolvimento/qa/controle-de-versao.html)
- [Code Review](desenvolvimento/qa/code-review.html)
- [Trabalhando com Issues](desenvolvimento/qa/trabalhando-com-issues.html)

## Referencias

[Git Basics (em ingles)](https://git-scm.com/book/pt-br/v2/Come%C3%A7ando-Git-Basics)

[Branches in a Nutshell (em ingles)](https://git-scm.com/book/pt-br/v2/Git-Branching-Branches-in-a-Nutshell)

[Branching Workflows (em ingles)](https://git-scm.com/book/pt-br/v2/Git-Branching-Branching-Workflows)

[Distribuited Workflows (em ingles)](https://git-scm.com/book/pt-br/v2/Distributed-Git-Distributed-Workflows)

[Gitlab Flow (em ingles)](https://docs.gitlab.com/ee/workflow/gitlab_flow.html)

[Feature Branch by Martin Fowler (em ingles)](https://martinfowler.com/bliki/FeatureBranch.html)

[Branches - Gitlab (em ingles)](https://docs.gitlab.com/ee/user/project/repository/branches/)

[GitLab Workflow: An Overview - Blog Post (em ingles)](https://about.gitlab.com/2016/10/25/gitlab-workflow-an-overview/)
