# Controle de Versão

Conforme o projeto evolui, vai ganhando novos releases e consequentemente *novas versões*. 

Para o armazenamento dos artefatos de projeto, a equipe utilizará a ferramenta GitLab (Controle de Versão Distribuído - DVCS). 
Utilizando as boas práticas para manter a marcação e o resgate de versões, a ramificação do projeto, o controle de histórico e a qualidade do produto final. 

Inserimos o seguinte formato, para o controle de versionamento: 

                                          <<ANO>>.<<MILESTONE>>.<<FIXES>>

**ANO:** representa o ano de lançamento do produto, recebendo o formato AAAA.

**MILESTONE:** representa a milestone do projeto, sendo inteiros não negativos.

**FIXES:**  representa as correções de falhas, sendo inteiros não negativos.

Todos os produtos serão lançados com esse formato de versão, seguindo o exemplo:  **2019.1.1**

O formato adotado vem de acordo com o segmento de versionamento de produto, onde o ano é atribuído ao release do projeto, logo em seguida acompanha o número da milestone no qual a funcionalidade pertence e por último as correções ou alterações realizadas nesta milestone. 
Compreender que o versionamento de software é parte integrante do desenvolvimento, é de fato importante! Pois, devemos tratar com o mesmo cuidado e atenção que damos as metodologias e padrões de projeto adotados. 

Vamos seguir este formato de versionamento, afim de, ganhar qualidade e controle na evolução do projeto!