# Execução de Testes


## **Por que testar?** 

Essa é a pergunta que sempre ecoa por aí, quando o assunto é qualidade de software. O teste é simplesmente a etapa necessária para fornecer um produto ou serviço de alta qualidade de forma confiável e eficiente. No desenvolvimento do software, são vários os motivos para criar e executar testes, mas vejamos os principais:

1. Ajuda a encontrar bugs;
2. Facilita a refatoração;
3. Gera a documentação;
4. Melhora o design do código;
5. Garante que o trabalho tenha qualidade.

## **Que tipo de teste implementar?**

Existem várias metodologias, técnicas, níveis e tipos de testes de software utilizados para alcançar a qualidade da solução desenvolvida. Devemos entender os testes como um reforço na qualidade do software. 

### **Metodologias:**        
As metodologias de teste de software auxiliam na execução do processo, criando uma documentação necessária. Vejamos alguns exemplos:  

**TDD** - Test Driven Development

Uma abordagem evolutiva onde o desenvolvedor cria os testes antes de escrever o código funcional necessário, seguindo as seguintes etapas:

Red: início, um teste que falha é escrito para uma determinada funcionalidade.  
Green: precisamos fazer o teste passar buscando a solução mais simples primeiramente.  
Refactor: deixar o design do código mais legível, eliminando redundância no código e acoplamentos.  


**BDD** - Behavior Driven Development

Com foco no comportamento de uma aplicação, visa integrar regras de negócio com linguagem de programação. Apresenta uma linguagem clara e objetiva como documento final, baseando a sua escrita no Gherkin com o famoso Given-When-Then (Dado-Quando-Então). 

*Dado* uma condição  
*Quando* eu executo uma ação  
*Então* o resultado é esse  

Dicas para escrever bons cenários:  
* Eles devem descrever o comportamento e não ser um passo a passo;
* Precisam seguir a linguagem da história do usuário e dos critérios de aceite;
* O time deve ser envolvido no processo;
* Os cenários devem ser independentes;
* Utilize tags para exportar exemplos.

Exemplos de ferramentas de BDD:       
BDD é um conceito independente de linguagem, existindo implementações para todas as linguagens. Cucumber é a mais conhecida e possui implementação para as linguagens Ruby, Java e outras linguagens, JBehave e Concordion (Java), Behave (Python) e SpecFlow (.NET) também são utilizadas.

## **Técnicas de Testes (Como Testar):**       
Tem como objetivo detectar a presença de erros no sistema testado, reduzindo a probabilidade de erros, retrabalho e prejuízos na qualidade final da entrega.    

**Caixa Preta (Teste Funcional):** orientado a dados, essa técnica é utilizada para verificar a saída dos dados usando diversos tipos de entradas. Avalia o comportamento externo do sistema, pode ser utilizada em todos os níveis de teste.  
**Caixa Branca (Teste Estrutural):** orientado à lógica, essa técnica é utilizada para validar a estrutura, avaliar a qualidade da estruturação e da implementação do código. Avalia o comportamento interno do sistema (métodos, classe e trechos isolados de código).   
**Caixa Cinza:** técnica utilizada para analisar a parte lógica mais a funcionalidade do sistema, fazendo uma comparação com a especificação dos requisitos e o que esta sendo desenvolvido.    
**Regressão:** técnica utilizada para aplicação de versões mais recentes do sistema, que garantem que novos defeitos não surjam em componentes já analisados.    

## **Níveis de Testes (Quando Testar):**  
Definem o momento do ciclo de vida do software em que são realizados os testes.  

**Unidade** - Teste de Funcionalidade  
O teste unitário tem por objetivo testar a menor parte testável do sistema (unidade) em geral, um método. Deve responder as seguintes perguntas:

O que eu estou testando?  
O que o método deveria fazer?  
Qual o seu atual retorno?  
O que eu espero que retorne?  

**Integração** - Teste de Componentes/serviço  
O teste de integração serve para dizer se sua aplicação esta respondendo corretamente ou não. Agrupa os métodos já testados no teste unitário. Tem como objetivo encontrar falhas na integração interna dos componentes do sistema.

**Sistema** -  Teste de perfomance/carga  
O Teste de sistema é realizado com o propósito de avaliar a qualidade externa do produto, na medida do possível a qualidade em uso. Agrupa todos os módulos e testa em conjunto com a interface gráfica.

**Exploratório** - Testes Manuais  
O Teste exploratório é realizado em paralelo com outras técnicas, em ambientes pouco testados pelos testes convencionais. É o processo básico que verifica se o software atingiu a necessidade do cliente.   


## **Tipos de Testes (O que testar):**  
Funcional: Garantem se a regra de negócio esta sendo atendida.    
Não-Funcionais:  Validam os aspectos tais como: a aparência, a facilidade de uso, a velocidade e a robustez do sistema. Dentre os testes não-funcionais, podemos citar:  
Teste de Volume: Verifica se o sistema suporta alto volumes de dados.     
Teste de Segurança: Ajudam a garantir o funcionamento da aplicação.      
Teste de Desempenho: Verifica se o comportamento para funções de transações ou de negócio.     
Teste de Compatibilidade: Valida o funcionamento do sistema em diferentes navegadores e hardwares.  

##  **Testando o que realmente é necessário**

Ao iniciar um projeto, precisamos analisar para compreender quais as metodologias e técnicas de teste utilizar. Ao conhecer as metodologias queremos utilizá-las como regra, e isso não é o correto, pois devemos entender que as metodologias de testes são modelos de aplicação. O planejamento dos testes ajuda nesse processo, pois é elaborado com base nos requisitos, ajudando a focar nas necessidades do projeto.

##  **Por que automatizar os testes?**

A automação dos testes garante a redução do tempo gasto com depuração e correção de bug. Tem como objetivo viabilizar o desenvolvimento ágil do software.  
Quando uma suíte de testes for lenta em sua execução a tendência é que ela seja negligenciada. Por outro lado, uma suíte rápida pode ser disparada por sua IDE toda vez que salvar um arquivo, por exemplo, dessa forma o feedbak é instantâneo e mais útil.  
É importante separar diferentes tipos de testes a serem executados em diferentes fases do desenvolvimento. Testes unitários podem ser executados a qualquer momento pois tendem a serem rápidos, testes de integração por outro lado tendem a depender de recursos externos e têm seu setup mais lento, por isso são melhor aproveitados se executados em ambiente automatizado, posteriormente ao desenvolvimento.
Em resumo diferentes tipos de testes trazem diferentes benefícios, e exigem diferentes condições para serem executados.   
Analise e planeje os diferentes tipos de testes no seu projeto, a fim de garantir a qualidade através deles sem comprometer sua produtividade!


##  **Dicas:**

- É importante saber qual problema acontece quando o teste quebra, por isso ao criar um conjunto de testes o código precisa ser legível, separados em etapas bem definidas e possuir um bom report.  
- Evite acoplamento. Quanto mais desacoplados seus testes, melhor. Isso evita a quebra em cascata, auxiliando na busca de erros, também auxilia  o seu design de código, garantindo algo modularizado e de bem mais fácil manutenção.  
- Um teste de cada vez. Sempre faça testes pequenos, em geral, um teste para um método ou mais testes para um mesmo método, nunca o contrário. Um teste jamais poderá testar mais de um método.
- Testes são códigos, para o programador nada é tão simples como testar seu próprio código.


### **Referências**

https://medium.com/snowman-labs/nunca-havera-tempo-para-testar-26ada09097de  
https://percy.io/  
https://pt.wikipedia.org/wiki/Teste_de_software