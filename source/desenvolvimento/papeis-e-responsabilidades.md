# Papéis e Responsabilidades no Desenvolvimento de Software na Snowman Labs




O objetivo desse documento é organizar e descrever os papeis, atividades e cerimônias no desenvolvimento de *software* que serão praticados na **Snowman Labs**.

As diferentes necessidades do desenvolvimento de *software* por times multidisciplinares exigem uma gama de cerimonias, atividades e convenções. Tais necessidade vêm da ação natural do caos, efeito que não é muito proveitoso no desenvolvimento de *software*. Por isso precisamos continuamente e conscientemente avaliarmos nossos objetivos e resultados para alinharmos nossas técnicas e ferramentas com as necessidades atuais.

Para que possamos usufruir de um processo de melhora contínua proponho aqui um modelo que nos permita garantir algumas qualidades mínimas, para que possamos a partir delas alcançar patamares maiores.

O modelo descrito nesse documento faz parte de um processo contínuo de melhoria, sendo assim qualquer um dos aspectos aqui descritos estão abertos a discussão.

Aqui trataremos dos diferentes papéis e responsabilidades envolvidos no desenvolvimento de *software* na **Snowman Labs**. Note que as responsabilidades estão organizadas em papéis, esse papéis podem futuro vir a definir cargos efetivos, para que possamos melhor categorizar e recompensar diferentes profissionais baseados na sua efetiva contribuição. 

Essa organização é primeiramente voltada ao estabelecimento de uma cultura tecnológica interna, não de mercado. Mas tenho certeza que o aprendizado e a prática dessas atividades seriam de grande valia para a área como um todo.

Todos os papéis serão descritos com seus nomes em inglês, pois são mais comuns de serem encontrados nessa língua. 


Como esse é um modelo pensado para a **Snowman Labs**, poderão haver divergências nas definições encontradas em outras publicações.

Se você tem interesse em assumir qualquer um dos papéis descritos procure o [**CTO**](https://gitlab.com/johnnywell), ele irá te guiar nesse processo.


## Scrum Master

O **Scrum Master** é o arauto, responsável por difundir as práticas do *Scrum* entre os times. Seu principal papel é garantir que todos entendam o motivo pelo qual estão praticando tais cerimônias. Outro aspecto desse papel é colher *feedback* dos times, para que as necessárias adaptações sejam feitas no processo a fim de melhorá-lo e tornado adequado as reais necessidades de cada time.

Dentre todas as práticas na **Snowman Labs** o *Scrum* é a mais maleável de todas, permitindo que diferentes times possuam variações das práticas propostas, e isso é muito importante para o sucesso de sua prática. Seguindo as boas práticas do [*Manifesto para o desenvolvimento ágil de software*](https://www.manifestoagil.com.br), deve-se priorizar as pessoas ao processo e dessa forma atender as suas necessidades.


## Tech Lead

O **Tech Lead** é o capitão de um time, ele assume o papel imprescindível de ser o intermediador dos *stakeholders* do projeto (ex. cliente, negócio, etc.). Seu principal desafio é manter o time técnico engajado, e para isso pode contar com a ajuda do **Scrum Master**. O **Tech Lead** é também um *stakeholder*, sua palavra tem o mesmo peso dos outros e sua decisão técnica deve ser respeitada.


## Stack Lead

O *Stack Lead* é o responsável por definir e manter uma *Stack* de tecnologia da **Snowman Labs**.

Uma *Stack* é o conjunto de tecnologias, bibliotecas, arquiteturas e práticas de uma área, dessa forma qualquer alteração em algum desses itens deve ser aprovada pelo respectivo *Stack Lead*. São exemplos de *Stacks* da **Snowman Labs**: *Python*, *JavaScript*, *iOS*, *Android*, *Design*.


## Maintainer

O *Maintainer* é responsável por aceitar *MR’s* em diferentes repositórios de código, garantindo que os padrões de qualidade estão sendo mantidos. Alguns desses padrões são: estilo de código, de mensagem de commit e MR, além de qualquer definição da *Stack*, etc. 


## Reviewer

O **Reviewer** é responsável pela revisão de *MR’s*. Sua revisão deve ser crítica e garantir que os padrões de qualidade estão sendo mantidos, como, estilo de código, clareza e simplicidade da implementação, qualidade e quantidade de testes. Ele deve guiar o *Dev* responsável pelo *MR* para que sejam feitas as alterações caso necessário e também pode envolver outros *Reviewers* caso precise de outra opinião sobre o assunto. O importante é haver a comunicação clara dos problemas encontrados e como podem ser corrigidos. Sempre que possível o *MR* deve ser testado pelo **Reviewer** para garantir seu funcionamento.


## Tester

O **Tester** é o último guardião da qualidade do *software*. Ele é responsável por executar testes mais completos de fluxo, e garantir que aspectos visuais e outros que não podem ser testados automaticamente cumpram os requisitos. Cabe ao **Tester** o
papel de decidir se algo está cumprindo todos os requisitos funcionais e não funcionais, critérios de aceitação e implantação.


## Developer

O **Developer** é o perfeito combatente, ágil e dotado de artimanhas para sobrepujar uma vasta gama de desafios. Sempre atual e implacável o *Developer* é responsável por empregar as técnicas definidas pelos *Stack Leads* para produzir diferentes tipos de software, sempre atento aos padrões de [qualidade de *software*](desenvolvimento/qa)

### Responsabilidades

- Analisar requisitos.
- Discutir opcões e viabilidade.
- Junto dos seus pares, especificar e estimar as atividades futuras.
- Implementar testes necessários para cumprir os critérios de cada feature, ou evitar regressão.
- Esclarecer, se necessário documentar, qualquer informação necessária para a execução das atividades de seus pares dependentes do seu trabalho.
- Disponibilizar os atrefatos de cada atividade para testes e homologação.