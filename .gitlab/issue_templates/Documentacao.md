<!--
* Use esse template de issue para sugerir novos documentos ou atualizar documentos existentes.
  Nota: Documentos de trabalho como um parte de desenvolvimento de uma funcionalidade é coberto 
        no template de Proposta de Funcionalidade.

* Para mais informações sobre documentação de conteudo e processo, veja
     https://handbook.snowmanlabs.coms/ -->

<!-- Tipo da issue -->

<!-- Descomente a linha para a documentação aplicavel ao tipo de issue para adicionar a label 
     Note que todos os textos nesta linha é deleteado depois da criação da issue -->
     
<!-- /label ~"docs:fix" - Correção ou esclarecimento necessário. -->
<!-- /label ~"docs:new" - Documentação nova necessária para cobrir um novo tópico ou um caso de uso. -->
<!-- /label ~"docs:improvement" - Melhorando uma documentação existente; ex.: adicionando um diagrama, adicionando ou refazendo uma texto, resolvendo redundancias, cross-linking, etc. -->
<!-- /label ~"docs:revamp" - Revisando uma página ou grupo de páginas em ordem para planejar e implementar maiores melhores/re-escritas. -->
<!-- /label ~"docs:other" - Anything else. -->

### Problema para resolver

<!-- Incluir o seguinte detalhe como necessário:
* Qual produto ou funcionalidade(s) afetado(s)?
* Quais documentos ou seção de documento afetado? Incluir links ou caminhos.
* Tem um problema com um documento especifico, ou uma funcionalidade/processo que não está suficientemente endereçado nos documentos?
* Qualquer outra ideia ou solicitação?
-->

### Mais Detalhes

<!--
* Qualquer conceito, procedimento, informações de referencia nós podemos adicionar para ficar mais fácil?
* Incluir casos de uso, beneficios, e/ou vantagens para este trabalho.
* Se adicionar conteudo: Qual publico será alcançado/afetado? (Quais roteiros e cenários?)
  Para ideias, veja as personas em https://design.gitlab.com/research/personas
-->

### Proposta

<!-- Especifique mais para como podemos resolver o problem. -->

### À quem se endereça essa issue

<!-- E precisa de algum conhecimento ou experiência para resolver esta issue? -->

### Outros links/referencias

<!-- Ex. issue/MRs relacionados informe aqui -->

/label ~docs
