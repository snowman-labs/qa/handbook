<!-- Este issue requer um escritor tecnico para documentar 
    conteudo que foi mergeado sem um. -->

<!-- NOTA: Por favor, atribua para um Tech Lead, DevOps, QA ou relacionados -->

## Referencias

MR Mergeado que introduzir documentação requer revisão:

Issue(s) Relacionadas:

## Mais detalhes

<!-- Qualquer contexto, pergunta ou notas adicionais para o escritor técnico -->

/label ~docs ~docs-review
