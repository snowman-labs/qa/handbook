## Descrição da Proposta

<!--
Por favor, descreva a proposta e adicione um link para a fonte (por exemplo, http://www.betterspecs.org/).
-->

- [ ] Mencione a proposta na próxima weekly e encoraje a contribuição no canal do slack #dev
- [ ] Proceda com a proposta se pelo menos %50 dos maintainers tiverem ponderado, e 80% dos votos são :+1
- [ ] Uma vez aprovado, mencione novamente na próxima weekly e no canal do slack #dev

/label ~"development guidelines"
/label ~"style decision"
/label ~docs
/label ~discussion

/cc @snowman-labs/dev
