### Problema para resolver

<!-- Qual problema resolver? -->

### Usuários alvo

<!-- Quem vai usar esta funcionalidade? Se souber, inclua qualquer um dos seguintes: tipo de usuário (ex. Desenvolvedor), 
    personas, ou função especifica da empresa (ex. Gerente de Release). Esteja livre pra escrever "Desconhecido" e preencha os campos abaixo.
Personas podem ser encontradas em https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/ -->

### Mais detalhes

<!-- Inclua casos de uso, beneficios, e/ou vantagens (contribue para nossa visão?) -->

### Proposta

<!-- Como nós vamos resolver o problema? Tente incluir a jornada do usuário! https://about.gitlab.com/handbook/journeys/#user-journey -->

### Permissões e Segurança

<!-- Quais permissões são necessárias para performar as ações descritas? Elas são consistentes com as permissões 
existentes documentadas para usuários, grupos, e projetos apropriadamente? Comportamento proposto é consistente entre UI, 
API, e outros metodos de acesso (ex. respostas de email)? -->

### Documentação

<!-- Veja o Fluxo de Documentação de Mudança de Funcionalidade/Feature https://docs.gitlab.com/ee/development/documentation/feature-change-workflow.html
-->

### Testes

<!-- Quais riscos esta mudança representa? Como pode afetar a qualidade do produto? 
Quais testes de cobertura adicionais ou mudanças de testes serão necessários? 
Vai precisar de teste cross-browser? Veja o processo de teste de software do 
Gitlab para lhe ajudar: https://about.gitlab.com/handbook/engineering/quality/guidelines/test-engineering/ -->

### Qual é o indicador de sucesso, e como podemos medi-lo?

<!-- Defina ambos as métricas de sucesso e os critérios de aceitação. Note que as metricas de sucesso indicam os 
retornos desejado para o negócio, enquanto que os critérios de aceitação indicam quando a solução está funcionando 
corretamente. Se não tem uma forma de mensurar o sucesso, link para uma issue que isto vai implementar um jeito de mensurar isso. -->

### Links / referencias

/label ~feature
