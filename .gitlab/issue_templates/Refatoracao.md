## Resumo

<!--
Por favor, descreva brevemente qual parte do código base precisa ser refatorada.
-->

## Melhorias

<!--
Explique os benefícios de refatorar esse código.
Veja também, https://about.gitlab.com/handbook/values/index.html#say-why-not-just-what
-->

## Riscos

<!--
Por favor, liste as funcionalidades que podem parar de funcionar por causa desse refatoramento 
e como você pretende resolver isso
-->

## Componentes envolvidos

<!--
Liste arquivos e diretórios que será alterados pelo refatoramento.
-->

## Opcional: Efeitos colaterais pretendidos

<!--
Se a refatoração envolve mudanças a parte das melhorias principais (como uma melhor UI), liste-as aqui.
Pode ser uma boa ideia para criar issues separadas e linka-las aqui.
-->

## Opcional: Cobertura de Teste ausente

<!--
Se você tem ciencia dos testes que precisam ser escritos ou ajustados como parte dos testes unitários 
para componentes alterados, por favor liste-os aqui.
-->

/label ~backstage
