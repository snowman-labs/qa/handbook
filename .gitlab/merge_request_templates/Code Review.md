## O que este MR faz?

<!-- Descreva brevemente sobre o que é este MR. -->

## Issues relacionadas

<!-- Link issues relacionadas a seguir. Insira o link da issue ou referencia depois da palavra "Closes" que ao mergear isto precisa ser automaticamente fechado. -->

## Preparando seu MR para Code Review

- [ ] Relacionar todas as issues que seu MR (Merge Request) esta correlacionado, marcando caso seja necessário fechá-las ou apenas citando-as.
- [ ] Adicionar toda informação necessária para explicar as coisas que esse MR altera.
- [ ] Se necessário, descrever como executar novos procedimentos (ex.: deploy, test, migrations, etc.)
- [ ] Se necessário, explicar os decisões tomadas e motivos.
- [ ] Marcar o(s) grupo(s) de reviewer(s) adequados (ex.: `@snowman-labs/be-devs`)
- [ ] Marcar como assignee o maintainer do projeto (ex.: o tech lead, ou outro dev do projeto com esse papel)
- [ ] Remover WIP (Work In Progress)
- [ ] Quando estiver realizando as mudanças solicitadas pelo Reviewer add o WIP (Work in Progress) novamente e remover quando terminar.

## Fazendo Code Review de um MR

- [ ] Inicie adicionando um comentário de que já está fazendo o Code Review do MR
- [ ] Buscar se já não tem outra issue e/ou MR relacionado, marcando-as para serem fechadas, ou apenas citando-as se não for o caso de fechar
- [ ] Caso não tenha passado nos requisitos do CI ou ainda esteja com WIP não analisar e informar o desenvolvedor sobre
- [ ] Analisar:
  - [ ] Adequação quanto as regras da Stack (code style, arquitetura, dependências, etc.)
  - [ ] Cumprimento do objetivo
  - [ ] Clareza da solução
  - [ ] Cobertura de testes
  - [ ] Pontos de melhoria
  - [ ] Possíveis problemas não previstos pela proposta
- [ ] Executar a aplicação e testar. Caso esse processo seja muito difícil discuta com o dev como esse processo pode ser melhorado e abra uma issue para isso.
- [ ] Quando terminar de revisar, faça um comentário marcando o desenvolvedor para que ele possa aplicar as mudanças
- [ ] Ao terminar o Code Review e tudo estando OK adicione um 👍 para indicar que aprovou tudo

## Referencias

<!-- Adicione referencias caso seja necessário -->

[Handbook - Code Review](https://snowman-labs.gitlab.io/qa/handbook/desenvolvimento/qa/code-review.html)

## Reviewers

<!-- Marque aqui quem vai ser o(s) Reviewer -->

Reviewer(s): @snowman-labs/be-devs <!-- pode adicionar pessoas especificas -->

<!-- Veja mais no Handbook sobre o processo de Code Review https://snowman-labs.gitlab.io/qa/handbook/desenvolvimento/qa/code-review.html -->