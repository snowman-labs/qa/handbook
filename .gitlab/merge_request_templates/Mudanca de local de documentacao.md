<!-- Veja o nosso handbook para maiores informações https://handbook.snowmanlabs.com/ -->

<!-- Use esse template de descrição para mudar a localização de documentação. Para novos documentos ou atualizações de documentos existentes, use o template "Documentation" -->

## O que este MR faz?

<!-- Descreva brevemente sobre o que é essa MR -->

## Issues relacionadas

<!-- Mencione a(s) issue(s) que este MR fecha ou está relacionado -->

Closes 

<!-- Ps. Closes é o termo usado pelo gitlab para fechar issues automaticamente quando fechar o MR. -->

## Movendo documentações para nova localização?

<!-- esse processo ainda sera melhor definido, mas veja abaixo o link do metodo do gitlab -->

Leia o guia:
https://docs.gitlab.com/ce/development/documentation/index.html#changing-document-location

- [ ] Tenha certeza que o link antigo não é removido e tenha o conteudo substituido com a nova localização de link.
- [ ] Tenha certeza que os links internos apontando para o documento em questão não estejam quebrados.
- [ ] Pesquise e substitua os links referenciados para documentos antigos.
- [ ] Tenha certeza de adicionar [`redirect_from`](https://docs.gitlab.com/ce/development/writing_documentation.html#redirections-for-pages-with-disqus-comments)
      para o novo documento se tem qualquer comentário do Disqus na página antiga do documento.
- [ ] Atualize o link na página inicial da documentação (se necessário)
- [ ] Marque um dos revisores/escritores técnicos para revisar. 

/label ~docs
