<!-- Siga o fluxo de documentação https://docs.gitlab.com/ee/development/documentation/workflow.html -->
<!-- Informações adicionais disponiveis aqui https://docs.gitlab.com/ee/development/documentation/ -->

<!-- Mencione "documentacao" ou "docs" no titulo do MR -->
<!-- Para mudança de localização de documentação use o template "Change documentation location" -->

## O que estea MR faz?

<!-- Descreva brevemente sobre o que é este MR. -->

## Issues relacionadas

<!-- Link issues relacionadas a seguir. Insira o link da issue ou referencia depois da palavra "Closes" que ao mergear isto precisa ser automaticamente fechado. -->

## Checklist do Autor

- [ ] Siga o [Guia de Documentaçao](https://docs.gitlab.com/ee/development/documentation/) e o [Guia de Estilo](https://docs.gitlab.com/ee/development/documentation/styleguide.html).
- [ ] Link docs para e de o mais alto nivel na página index, adicione outros docs relacionados que sejam uteis.
- [ ] Aplicar a label ~docs.

## Checklist de Revisão

Todos os revisores pode ajudar a garantir a precisão, clareza, integridade e aderencia para o [Guia de Documentação](https://docs.gitlab.com/ee/development/documentation/) e o [Guia de Estilo](https://docs.gitlab.com/ee/development/documentation/styleguide.html).

**1. Revisor Primario**

* [ ] Revisto por um revisor de código ou outro colega selecionado para confirmar a precisão, clareza, integridade e aderencia. 
      Isto pode ser ignorado para correções menores sem conteúdo substantivo para ser mudado.

**2. Escritor Técnico**

* [ ] Opcional: Revisão do escritor técnico. Se não solicitado para este MR, precisa ser agendado para depois do merge. Solicitar para este MR, atribua o escritor.

**3. Maintainer**

1. [ ] Revisão pelo maintainer atribuido, quem pode sempre solicitar/exigir as revisões acima. Revisão do maintainer pode ocorrer antes ou depois da revisão do escritor técnico.
1. [ ] Garantir o milestone do release está configurado e que pode mergear MR equivalentes.
1. [ ] Se não tem uma revisão de um escritor técnico, crie uma issue para um usando o template "Doc Review"

/label ~docs
