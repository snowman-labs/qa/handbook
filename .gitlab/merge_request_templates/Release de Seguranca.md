<!--
# LEIAME Primeiro

Veja o [guia de release de segurança do desenvolvedor](https://gitlab.com/gitlab-org/release/docs/blob/master/general/security/developer.md).

Este merge request _nao precisa_ fechar a issue de segurança correspondente _a menos que_ isso seja na master

-->
## Issues relacionadas

<!-- Mencione a(s) issue(s) que esta MR está relacionada -->

## Checklist do Desenvolvedor

- [ ] Link para a issue do fluxo de segurança do desenvolvedor, caso exista
- [ ] MR alvo para `master`, ou `X-Y-stable` para backports
- [ ] Milestone está configurada para a versão que está MR se aplica
- [ ] Titulo desta MR é o mesmo para todos os backports
- [ ] Uma [entrada de CHANGELOG](https://docs.gitlab.com/ee/development/changelog.html) é adicionada sem um valor de `merge_request`, com `type` configurado para `security`
- [ ] Adicione um link para este MR na seção de `links` da issue relacionada
- [ ] Atribuir para um revisor (este não é um release manager)

## Checklist do Revisor

- [ ] Aplicado a milestone correta e o titulo compativel com todos os backports
- [ ] Verificar se passa nas pipelines de CI

/label ~security
