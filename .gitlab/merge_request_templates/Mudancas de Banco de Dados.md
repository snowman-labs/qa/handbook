## O que esse MR faz?

<!--
Descreva em detalhes o que seu merge request faz, porque isso é feito, etc. Merge
Requests sem uma descrição adequada não serão revisados até uma descrição ser
adicionada.

Por favor também mantenha esta descrição atualizada com qualquer discussão que tenha 
feito para que os revisores possam entender o que você pretende fazer. Isso é 
muito importante para o caso de eles não participarem da discussão.

Tenha certeza de remover este comentário quando você terminar.
-->

<!-- PS. esse processo ainda será definido, em caso de duvidas entre em contato no canal do slack
https://snowmanlabs.slack.com/messages/C0299TJ97 -->

Adicione uma descrição do seu merge request aqui.

## Checklist de Banco de dados/Database

- [ ] Verifique o [guia de banco de dados](https://docs.gitlab.com/ee/development/README.html#database-guides)

Quando adicionar migrations:

- [ ] Atualizar com `bin/migrate.sh`
- [ ] Foi adicionado metodo de `down` para que a migration possa ser revertida
- [ ] Foi adicionado a saida da(s) migration(s) para o corpo do MR
- [ ] Foi adicionado testes para a migration se necessário (ex. quando tem migração de dados)

Quando adicionar ou modificar consultas para melhorar performance:

- [ ] Incluido dados que mostram a melhoria de performance, preferivelmente na forma de uma benchmark
- [ ] Incluido a saida de consultas relevantes como `EXPLAIN (ANALYZE, BUFFERS)`

Quando adicionar chaves estrangeiras (foreing keys) para as tabelas existentes:

- [ ] Incluido uma migração para remover linhas orfãs na tabela de origem antes de adicionar a chave estrangeira
- [ ] Removido qualquer instancia de dependencias que podem não ser mais necessárias

Quando acionar tabelas:

- [ ] Ordenação de columas baseada na [Tabela de Ordenação de Colunas](https://docs.gitlab.com/ee/development/ordering_table_columns.html#ordering-table-columns)
- [ ] Adicionado chaves estrangeiras para qualquer coluna que aponta para dados de outras tabelas
- [ ] Adicionado indexes para campos que são usados em consultas com WHERE, ORDER BY, GROUP BY, e JOINS

Quando remover colunas, tabelas, indexes e outras estruturas:

- [ ] Removido migração em post-deployment
- [ ] Tenha certeza que esta aplicação não é mais usada (ou ignora) estas estruturas

## Checklist geral

- [ ] Adicionado [Entrada no Changelog](https://docs.gitlab.com/ee/development/changelog.html), se necessário
- [ ] [Criado/atualizado documentação](https://docs.gitlab.com/ee/development/documentation/)
- [ ] [Adicionado testes para esta feature/bug](https://docs.gitlab.com/ee/development/testing_guide/index.html)
- [ ] Está conforme o [guia de code review](https://docs.gitlab.com/ee/development/code_review.html)
- [ ] Está conforme o [guia de performance do merge request](https://docs.gitlab.com/ee/development/merge_request_performance_guidelines.html)
- [ ] Está conforme o [guia de estilos](https://docs.gitlab.com/ee/development/contributing/style_guides.html)

/label ~database
